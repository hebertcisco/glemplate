//// Helper utilities for result types.

import gleam/list.{Continue, Stop}

/// Fold over a list, stopping at the first error encountered.
pub fn fold(
  items: List(a),
  init: b,
  folder: fn(b, a) -> Result(b, c),
) -> Result(b, c) {
  list.fold_until(
    items,
    Ok(init),
    fn(acc, item) {
      let assert Ok(acc) = acc
      let res = folder(acc, item)

      case res {
        Ok(next_acc) -> Continue(Ok(next_acc))
        Error(error) -> Stop(Error(error))
      }
    },
  )
}
