import gleam/map
import gleam/string_builder
import gleeunit/should
import glemplate/parser
import glemplate/html
import glemplate/assigns

pub fn encoding_test() {
  let template = "<b>Welcome, <%= name %>!</b> <%= raw name %>"

  let assert Ok(tpl) = parser.parse_to_template(template, "input.html.glemp")

  let template_cache = map.new()
  let assigns =
    assigns.new()
    |> assigns.add_string("name", "||<xXx_KillaBoy_69>||")

  let assert Ok(result) = html.render(tpl, assigns, template_cache)

  should.equal(
    string_builder.to_string(result),
    "<b>Welcome, ||&lt;xXx_KillaBoy_69&gt;||!</b> ||<xXx_KillaBoy_69>||",
  )
}
